#!/usr/bin/env groovy

/*
  Copyright (C) 2017 Collabora Limited
  Author: Guillaume Tucker <guillaume.tucker@collabora.com>

  This module is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  This library is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
  details.

  You should have received a copy of the GNU Lesser General Public License
  along with this library; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/* Note: docker_image.inside() needs to be called after dir(some_directory)
 * otherwise it fails and stays at the root of the workspace. */

def REGISTRY = "auth.docker-registry.collabora.com"
def REGISTRY_CREDENTIALS = "ccu-docker-collabora-com"
def IMAGE_NAME = "debos"

node("docker-slave") {
    docker.withRegistry("https://${REGISTRY}", REGISTRY_CREDENTIALS) {
        def docker_image = null
        def checkout = env.WORKSPACE + "/checkout"

        echo """\
registry: ${REGISTRY}
image:    ${IMAGE_NAME}
"""

        stage("job checkout") {
            dir(checkout) {
                git(url: env.gitlabSourceRepoHttpUrl,
                    branch: env.gitlabBranch,
                    poll: false)
            }
        }

        stage("build") {
            dir(checkout) {
                docker_image = docker.build(
                    "${REGISTRY}/${IMAGE_NAME}",
                    "--no-cache docker")
            }
        }

        stage("push") {
            if (env.gitlabBranch == "master") {
              docker_image.push()
            }
        }
    }
}
